#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <errno.h>

#define DELIMETER (" ")
#define FAVORITE_ARTISTS ("Yoni Eric HaDorbanim Jimbo Hoodyman")
#define BUFF_SIZE (100)

enum {
    READ,
    WRITE
};

enum {
    FAIL = -1,
    SUCCESS
};

/**
 * @brief takes a sentence and prints every word on a new line
 * 
 * @param sentence - the sentence
 * @return int - SUCCESS on success, FAIL otherwise
 */
int print_by_delimeter(char * sentence, const char * delimeter)
{
    char * word = NULL;

    word = strtok(sentence, delimeter);

    while (word != NULL)
    {
        printf("%s\n", word);
        word = strtok(NULL, delimeter);
    }

    return SUCCESS;
}

int read_all(int fd, char * buffer, size_t size)
{
    ssize_t bytes_read = -1;

    while (size != 0 && bytes_read != 0)
    {
        bytes_read = read(fd, buffer, size);
        
        if (0 > bytes_read)
        {
            return FAIL;
        }

        size -= bytes_read;
        buffer += bytes_read;
    }

    return SUCCESS;
}

int write_all(int fd, char * buffer, size_t size)
{
    ssize_t bytes_written = -1;

    while (size != 0)
    {
        bytes_written = write(fd, buffer, size);

        if (0 > bytes_written)
        {
            return FAIL;
        }

        size -= bytes_written;
        buffer +=  bytes_written;
    }

    return SUCCESS;
}

int main(void)
{
    int pipefd[2] = {-1, -1};
    int return_code = FAIL;
    int status = 0;
    pid_t child_pid = -1;

    char buffer[BUFF_SIZE] = {0}; 
    

    if (0 > pipe(pipefd))
    {
        perror("pipe");
        goto cleanup;
    }

    child_pid = fork();

    switch(child_pid)
    {
        case -1:
            perror("fork");
            goto cleanup;
            break;

        case 0:
            close(pipefd[WRITE]);

            if (0 > read_all(pipefd[READ], buffer, BUFF_SIZE))
            {
                perror("read");
                goto cleanup;
            }

            print_by_delimeter(buffer, DELIMETER);
            break;

        default:
            close(pipefd[READ]);

            if (0 > write_all(pipefd[WRITE], FAVORITE_ARTISTS, sizeof(FAVORITE_ARTISTS)))
            {
                perror("write");
                goto cleanup;
            }
            
            close(pipefd[WRITE]);

            wait(&status);

            if (WIFEXITED(status) != 1)
            {
                goto cleanup;
            }

            break;
    }

    return_code = SUCCESS;

cleanup:
    for (int i = 0; i < sizeof(pipefd); ++i)
    {
        if (pipefd[0] != -1)
        {
            close(pipefd[0]);
            pipefd[0] = -1;
        }
    }
    return return_code;
}
