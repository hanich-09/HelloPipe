CC = gcc
CFLAGS = -g -Wall -Werror
INC = inc
LIB = lib
SRC = src
BIN = bin
EXEC = main

$(EXEC): $(EXEC).o
	mkdir -p $(BIN)/
	$(CC) $(CFLAGS) $(EXEC).o -I$(INC) -L$(LIB) -o $(BIN)/$(EXEC)

$(EXEC).o: $(SRC)/$(EXEC).c
	$(CC) $(CFLAGS) -c $(SRC)/$(EXEC).c -I$(INC) -L$(LIB)

clean:
	rm -rf $(BIN)
	rm -f *.o